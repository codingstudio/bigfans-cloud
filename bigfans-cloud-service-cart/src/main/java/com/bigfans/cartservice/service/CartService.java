package com.bigfans.cartservice.service;

import com.bigfans.cartservice.model.Cart;
import com.bigfans.cartservice.model.CartItem;
import com.bigfans.framework.dao.BaseService;

import java.util.List;

/**
 * 
 * @Description:
 * @author lichong 2015年3月24日上午11:40:32
 *
 */
public interface CartService extends BaseService<CartItem> {

	Cart myCart(String userId) throws Exception;
	
	Cart myCart(String userId, Long start, Long pagesize) throws Exception;
	
	Integer sumMyCart(String userId) throws Exception;
	
	Cart cartSummary(String userId, Long pagesize) throws Exception;

	void mergeMyCart(String usreId, Cart source) throws Exception;

	void removeItem(String userId, String id) throws Exception;
	
	void removeItems(String userId, String[] ids) throws Exception;
	
	void removeCart(String userId) throws Exception;

	void addItem(String userId, String pid, Integer quantity) throws Exception;

	void updateQuantity(String uid, String pid, int quantity) throws Exception;
	
	void select(String uid, String productId) throws Exception;
	
	void selectAll(String uid) throws Exception;
	
	void cancel(String uid, String productId) throws Exception;
	
	void cancelAll(String uid) throws Exception;
	
	Integer countByProduct(String uid, String pid) throws Exception;
	
	List<CartItem> getItems(String userId) throws Exception;
	
	List<CartItem> getSelectedItems(String userId) throws Exception;
	
	void removeSelectedItems(String userId) throws Exception;
	
	Cart populateCart(List<CartItem> cartItems) throws Exception;
}
