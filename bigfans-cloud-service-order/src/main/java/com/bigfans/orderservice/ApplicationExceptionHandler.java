package com.bigfans.orderservice;

import com.bigfans.framework.exception.GlobalExceptionHandler;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.exception.OrderCreateException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ApplicationExceptionHandler extends GlobalExceptionHandler{

    @ExceptionHandler(value = OrderCreateException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public RestResponse orderCreateExceptionHandler(OrderCreateException ex){
        return RestResponse.error(ex.getMessage());
    }

}
