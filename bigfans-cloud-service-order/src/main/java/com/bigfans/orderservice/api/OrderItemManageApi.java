package com.bigfans.orderservice.api;

import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import com.bigfans.orderservice.model.OrderItem;
import com.bigfans.orderservice.service.OrderItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lichong
 * @create 2018-04-14 上午8:57
 **/
@RestController
public class OrderItemManageApi extends BaseController {

    @Autowired
    private OrderItemService orderItemService;

    @GetMapping(value = "/orderItems")
    public RestResponse listItems(@RequestParam(value = "orderId") String orderId) throws Exception{
        List<OrderItem> orderItems = orderItemService.listByOrder(orderId);
        return RestResponse.ok(orderItems);
    }

}
