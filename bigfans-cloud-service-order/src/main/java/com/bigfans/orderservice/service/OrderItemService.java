package com.bigfans.orderservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.model.PageBean;
import com.bigfans.orderservice.model.OrderItem;

import java.util.List;

public interface OrderItemService extends BaseService<OrderItem> {
	
	/**
	 * 更新评论状态
	 * @param orderItemId
	 */
	void updateStatusToCommented(String orderItemId) throws Exception;
	
	List<OrderItem> listByUserOrder(String userId, String orderId) throws Exception;

	List<OrderItem> listByOrder(String orderId) throws Exception;

	List<OrderItem> listUnCommentedItems(String userId, String orderId) throws Exception;
	
}
