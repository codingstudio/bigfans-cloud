package com.bigfans.framework.dao.mybatis;

public class MySqlDialect extends Dialect {

	@Override
	public String getPagerSql(String originalSql, Long start, Long pagesize) {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from (")
		  .append(originalSql)
		  .append(") t LIMIT ")
		  .append(start)
		  .append(" , ")
		  .append(pagesize);
		return sb.toString();
	}
}
