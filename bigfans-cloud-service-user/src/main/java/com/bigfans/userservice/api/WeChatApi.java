package com.bigfans.userservice.api;

import com.bigfans.framework.web.BaseController;
import com.bigfans.framework.web.RestResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lichong
 * @create 2018-08-04 下午8:45
 **/
@RestController("/wechat")
public class WeChatApi extends BaseController {

    private String appId = "wxb394f3ec882dfc42";

    @GetMapping("/exchangeOpenId")
    public RestResponse exchangeOpenId(){
        return RestResponse.ok();
    }

}
